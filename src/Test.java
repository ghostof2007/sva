import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ghostof2007 on 5/31/14.
 *
 */
public class Test {

    Viterbi viterbi = new Viterbi();

    void testViterbi() {
        double[] timePoints = new double[]{0, 1.0, 1.33, 2.33};
        double[][] transitionProb = new double[][]
                {{0,0.1,0.9},
                 {0.1,0,0.9},
                 {0.9,0.1,0}};
        double[] rateVector = new double[]{3,2,1};
        double[] initProb = new double[]{0.33,0.33,1-0.66};
        double[][] state2ObsProb = new double[][]
                {{0.98,0.01,0.01},
                 {0.01,0.98,0.01},
                 {0.01,0.01,0.98}};
        ArrayList<Pair<Integer, Double>> obsAndTime = new ArrayList<Pair<Integer, Double>>();
        obsAndTime.add(new MutablePair<Integer, Double>(0, 1.1));
        obsAndTime.add(new MutablePair<Integer, Double>(1, 1.5));

        viterbi.getStateSequence(timePoints,
        transitionProb, rateVector,initProb, state2ObsProb,obsAndTime);
    }

    void testOpt() throws Exception {
        Pair<Double, double[]> returnVal = SVA.optimizeTime(new double[]{1,2,3}, 1);
        System.err.println(returnVal.getLeft() + " : " + Arrays.toString(returnVal.getRight()));
    }
}
