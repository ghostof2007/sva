import com.joptimizer.functions.ConvexMultivariateRealFunction;
import com.joptimizer.functions.LinearMultivariateRealFunction;
import com.joptimizer.optimizers.JOptimizer;
import com.joptimizer.optimizers.OptimizationRequest;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Created by ghostof2007 on 5/31/14.
 */
public class SVA {

    static Pair<Double, double[]> optimizeTime(final double [] lambdaList, double totalTime) throws Exception {
        //returns timeVector of the same length as rateVector

        // Objective function
        ConvexMultivariateRealFunction
                objectiveFunction = new ConvexMultivariateRealFunction() {

            public double value(double[] X) {
                double x = 0;
                for(int i=0;i<X.length-1; i++) {
                    double lambdaTime = lambdaList[i] * X[i];
                    x += lambdaTime - Math.log(lambdaTime) - 1;
                }
                return x;
            }

            public double[] gradient(double[] X) {
                double [] grad = new double[X.length];
                for(int i=0;i<X.length;i++)
                    grad[i] = lambdaList[i] - 1/X[i];
                return grad;
            }

            public double[][] hessian(double[] X) {
                double [][] hess = new double[X.length][X.length];
                for(int i=0;i<X.length;i++)
                    hess[i][i] = 1/(X[i]*X[i]);
                return hess;
            }

            public int getDim() {
                return lambdaList.length;
            }
        };

        ConvexMultivariateRealFunction[] inequalities = new ConvexMultivariateRealFunction[2];
        double [] G = new double[lambdaList.length];
        for(int i=0;i<G.length;i++)
            G[i] = 1;
        inequalities[0] = new LinearMultivariateRealFunction(G, -totalTime-1e-2);
        for(int i=0;i<G.length;i++)
            G[i] = -1;
        inequalities[1] = new LinearMultivariateRealFunction(G, totalTime - 1e-2);

        OptimizationRequest or = new OptimizationRequest();
        or.setF0(objectiveFunction);
        or.setFi(inequalities);
        double []initPoint = new double[lambdaList.length];
        for(int i=0;i<initPoint.length;i++)
            initPoint[i] = totalTime/initPoint.length;
        or.setInitialPoint(initPoint);

        // optimization
        JOptimizer opt = new JOptimizer();
        opt.setOptimizationRequest(or);
        int returnCode = opt.optimize();

        double [] solution = opt.getOptimizationResponse().getSolution();
        double solValue = objectiveFunction.value(solution);
        return new MutablePair<Double, double[]>(solValue, solution);


    }
}
