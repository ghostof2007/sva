import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;

/**
 * Created by ghostof2007 on 5/31/14.
 * OPtimization function calc
 */
public class Function {

    static double function(double [] timePoints, int[] states,
                           double [][] transitionProb, double [] rateVector,
                           double [] initProb, double [][] state2ObsProb,
                           ArrayList<Pair<Integer, Double>> obsAndTime) {
        //return the value of the function given the variables
        double f = 0.;
        f -= Math.log(initProb[states[0]]);
        int i;
        for(i=0;i<timePoints.length-2; i++) {
            f -= Math.log(transitionProb[states[i]][states[i + 1]]);
            double lambdaTime = rateVector[states[i]] * (timePoints[i+1]-timePoints[i]);
            f += (lambdaTime - Math.log(lambdaTime) -1);
        }

        //last term of time part
        if((timePoints[i+1]-timePoints[i]) * rateVector[states[i]] > 1) {
            double lambdaTime = rateVector[states[i]] * (timePoints[i+1]-timePoints[i]);
            f += (lambdaTime - Math.log(lambdaTime) -1);
        }


        //TODO for each observation, add the LL term after calculating the corresponding state
        i=0;
        //assume observations are in chronological order
        for(Pair<Integer, Double> obs : obsAndTime) {
            while(obs.getRight() > timePoints[i])
                i++;
            //use state[i-1] now
            f -= Math.log(state2ObsProb[states[i-1]][obs.getLeft()]);
        }
        return f;
    }


}
