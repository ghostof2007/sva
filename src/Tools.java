import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by ghostof2007 on 5/6/14.
 *
 * Tools
 */
public class Tools {

    public static double max(double[] values) {
        double max = - Double.MAX_VALUE;
        for(double value : values) {
            if(value > max)
                max = value;
        }
        return max;
    }

    public static double logSumOfExponentials(double [] xs) {
        if (xs.length == 1) return xs[0];
        double max = max(xs);
        double sum = 0.0;
        for (double x : xs)
            if (x != Double.NEGATIVE_INFINITY)
                sum += Math.exp(x - max);
        return max + java.lang.Math.log(sum);
    }

    public static double logSumOfExponentials(ArrayList<Double> x) {
        double [] xs = new double[x.size()];
        for(int i=0;i<x.size(); i++)
            xs[i] = x.get(i);
        return logSumOfExponentials(xs);
    }

    //add values from one map to another (weighted by factor)
    static void updateMap(HashMap<Integer, Double> a, HashMap<Integer, Double> b, double factor) {
        for(int key : b.keySet()) {
            if (a.containsKey(key))
                a.put(key, a.get(key) + b.get(key) * factor);
            else
                a.put(key, b.get(key) * factor);
        }
    }

    static void updateMap(HashMap<Integer, Double> a, HashMap<Integer, Double> b) {
        updateMap(a, b, 1.);
    }

    static double sum(double [] array){
        double sum = 0.;
        for (double anArray : array) sum += anArray;
        return sum;
    }

    static double squareSum(double [] array){
        double sum = 0.;
        for (double anArray : array) sum += anArray * anArray;
        return sum;
    }



    //descending sort
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map )
    {
        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<K, V>>()
        {
            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
            {
                return -(o1.getValue()).compareTo( o2.getValue() ); //change sign to make ascending
            }
        } );

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list)
        {
            result.put( entry.getKey(), entry.getValue() );
        }
        return result;
    }


    static void incrementMap(Map<Integer, Integer> map, int key) {
        Integer value = map.get(key);
        if(value==null)
            map.put(key,1);
        else
            map.put(key, value+1);
    }


}
