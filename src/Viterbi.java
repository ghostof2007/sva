import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ghostof2007 on 5/31/14.
 *
 */
public class Viterbi {

    //find the most probable state sequence
    MutablePair<Double, ArrayList<Integer>> getStateSequence(double[] timePoints,
                                                             double[][] transitionProb, double[] rateVector,
                                                             double[] initProb, double[][] state2ObsProb,
                                                             ArrayList<Pair<Integer, Double>> obsAndTime){
        HashMap<Integer, ArrayList<Integer>> state2bestPath = new HashMap<Integer, ArrayList<Integer>>();
        HashMap<Integer, Double> state2bestCost = new HashMap<Integer, Double>();

        int currentObs = 0;
        //init
        for(int i=0;i<rateVector.length; i++) {
            state2bestPath.put(i, new ArrayList<Integer>());
            state2bestPath.get(i).add(i);
            state2bestCost.put(i, -Math.log(initProb[i]));
        }

        //TODO check last case
        //iteratively update the frontier
        for(int j=1;j<timePoints.length;j++) {

            //first process observations till the current time point
            while(currentObs < obsAndTime.size() && obsAndTime.get(currentObs).getRight() < timePoints[j]) {
                //update all the costs for the states
                for(int i=0;i<rateVector.length; i++) {
                    double oldVal = state2bestCost.get(i);
                    state2bestCost.put(i, oldVal
                            - Math.log(state2ObsProb[i][obsAndTime.get(currentObs).getLeft()]));
                }
                currentObs++;
            }

            //time part update
            for(int i=0;i<rateVector.length; i++) {
                double oldVal = state2bestCost.get(i);
                double lambdaTime = rateVector[i] * (timePoints[j]-timePoints[j-1]);
                if(j < timePoints.length-1 || (timePoints[j]-timePoints[j-1]) * rateVector[i] > 1)
                    state2bestCost.put(i, oldVal + lambdaTime - Math.log(lambdaTime) - 1);
            }

            //stop updating frontier if you are at second last point
            if(j == timePoints.length-1) break;

            //update frontier at the current time point
            HashMap<Integer, ArrayList<Integer>> newState2bestPath = new HashMap<Integer, ArrayList<Integer>>();
            HashMap<Integer, Double> newState2bestCost = new HashMap<Integer, Double>();
            for(int newState=0;newState<rateVector.length; newState++) {
                //go over all states from frontier and choose the best possible match
                double bestScore = Double.MAX_VALUE;
                int bestPrevState = 0;
                for(int prevState=0;prevState<rateVector.length; prevState++) {
                    if(prevState==newState) continue;
                    double newScore = state2bestCost.get(prevState) - Math.log(transitionProb[prevState][newState]);
                    if(newScore < bestScore) {
                        bestScore = newScore;
                        bestPrevState = prevState;
                    }
                }
                //update the new frontier
                ArrayList<Integer> tmpList = new ArrayList<Integer>();
                tmpList.addAll(state2bestPath.get(bestPrevState));
                tmpList.add(newState);
                newState2bestPath.put(newState, tmpList);
                newState2bestCost.put(newState, bestScore);
            }

            //update the frontier hashmap
            state2bestCost = newState2bestCost;
            state2bestPath = newState2bestPath;
        }

        //find the best scoring path
        double bestScore = Double.MAX_VALUE;
        int bestLastState = 0;
        for(int i=0;i<rateVector.length; i++)
            if(state2bestCost.get(i) < bestScore) {
                bestScore = state2bestCost.get(i);
                bestLastState = i;
            }

        ArrayList<Integer> bestPath = state2bestPath.get(bestLastState);
        bestPath.add(bestLastState);

        return new MutablePair<Double, ArrayList<Integer> >(bestScore, bestPath);
    }

}
